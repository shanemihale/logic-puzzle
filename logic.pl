/*
   logic.pl
   Solves the It's A Tie logic puzzle.

   Shane Hale
   smh1931@cs.rit.edu
*/

/* Properties and Values */
pattern(cupids).
pattern(faces).
pattern(leprechauns).
pattern(reindeer).
man(crow).
man(evans).
man(hurley).
man(speigler).
relative(daughter).
relative(fatherinlaw).
relative(sister).
relative(uncle).

/* Constraints */

/* 
   hint 1 
   a(pattern, relative)
*/
a(leprechauns, R) :- R \= daughter.

/* 
   hint 2 
   b(man, pattern)
*/
b(crow, P) :- P \= reindeer, P \= faces.

/* 
   hint 3 
   c(man, relative)
*/
c(speigler, R) :- R \= uncle.

/* 
   hint 4 
   d(tie, relative)
*/
d(faces, R) :- R \= sister.

/* 
   hint 5 
   e(man, pattern)
   f(man, relative)
   g(pattern, relative)
*/
e(M, leprechauns) :- M \= crow, M \= hurley.
f(M, fatherinlaw) :- M \= crow, M \= hurley.
g(leprechauns, R) :- R \= fatherinlaw.

/* 
   hint 6 
   g(man, relative)
*/
h(hurley, sister).

/*
   Solve the logic puzzle, and print the results.
*/
solve(M1,T1,R1, M2,T2,R2, M3,T3,R3, M4,T4,R4) :-
  
  /* freeze the man properties */
  M1 = crow,
  M2 = evans,
  M3 = hurley,
  M4 = speigler,

  /* make the properties distinct */
  pattern(T1),
  pattern(T2),
  pattern(T3),
  pattern(T4),
  relative(R1),
  relative(R2),
  relative(R3),
  relative(R4),

  T1 \= T2, T1 \= T3, T1 \= T4,
  T2 \= T3, T2 \= T4, T3 \= T4,

  R1 \= R2, R1 \= R3, R1 \= R4,
  R2 \= R3, R2 \= R4, R3 \= R4,

  ( a(T1, R1);
    a(T2, R2);
    a(T3, R3);
    a(T4, R4) ),

  ( b(M1, T1);
    b(M2, T2);
    b(M3, T3);
    b(M4, T4) ),

  ( c(M1, R1);
    c(M2, R2);
    c(M3, R3);
    c(M4, R4) ),

  ( d(T1, R1);
    d(T2, R2);
    d(T3, R3);
    d(T4, R4) ),

  ( e(M1, T1);
    e(M2, T2);
    e(M3, T3);
    e(M4, T4) ),

  ( f(M1, R1);
    f(M2, R2);
    f(M3, R3);
    f(M4, R4) ),

  ( g(T1, R1);
    g(T2, R2);
    g(T3, R3);
    g(T4, R4) ),

  ( h(M1, R1);
    h(M2, R2);
    h(M3, R3);
    h(M4, R4) ).

/*
   Main method, call solve
*/
main :-
  solve(M1,T1,R1, M2,T2,R2, M3,T3,R3, M4,T4,R4),

  /*
      Write out the results
  */
  write(M1), write('\t'),
  write(T1), write('\t'),
  write(R1), write('\n'),

  write(M2), write('\t'),
  write(T2), write('\t'),
  write(R2), write('\n'),

  write(M3), write('\t'),
  write(T3), write('\t'),
  write(R3), write('\n'),

  write(M4), write('\t'),
  write(T4), write('\t'),
  write(R4), write('\n'),

  fail.
